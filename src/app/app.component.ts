import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from './validations/must-match.validate';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  registerForm: FormGroup;

  Data: Array<any> = [
    { name: 'A Firent or colleage', value: 'A Firent or colleage' },
    { name: 'Google', value: 'Google' },
    { name: 'Blog Post', value: 'Blog Post' },
    { name: 'News Article', value: 'News Article' }
  ];

  constructor(private fb: FormBuilder) {
    this.registerForm = this.fb.group({
      fullName: this.fb.group({
        firstName: ['', [Validators.required, Validators.maxLength(32)]],
        lastName: ['', [Validators.required, Validators.maxLength(32)]],
      }),
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
        ],
      ],
      passwordComfirm: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
        ],
      ],
      phoneNumber: this.fb.group({
        code: ['',[Validators.required,Validators.pattern]],
        phone: ['',[Validators.required,Validators.pattern]]
      }),
      address: this.fb.group({
        address1: ['',Validators.required],
        address2: ['',Validators.required],
        city: ['',Validators.required],
        province: ['',Validators.required],
        zipCode: ['',[Validators.required,Validators.pattern]],
        country: ['',Validators.required]
      }),
      birth: this.fb.group({
        month: ['',Validators.required],
        day: ['',Validators.required],
        year: ['',Validators.required]
      }),
      checkArray: this.fb.array([], [Validators.required]),
      membership: ['',Validators.requiredTrue],
      privacy: ['',Validators.requiredTrue]
    },{
      validator: MustMatch('password', 'passwordComfirm')
    });
  }

  onSubmit() {
    // alert(JSON.stringify(this.registerForm.value));
  }

  onCheckboxChange(e) {
    const checkArray: FormArray = this.registerForm.get('checkArray') as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  get membership() {
    return this.registerForm.get('membership');
  }

  get privacy() {
    return this.registerForm.get('privacy');
  }

  get firstName() {
    return this.registerForm.get('fullName').get('firstName');
  }
 
  get lastName() {
    return this.registerForm.get('fullName').get('lastName');
  }

  get email(){
    return this.registerForm.get('email');
  }

  get password(){
    return this.registerForm.get('password');
  }

  get passwordComfirm(){
    return this.registerForm.get('passwordComfirm')
  }

  get code(){
    return this.registerForm.get('phoneNumber').get('code');
  }

  get phone(){
    return this.registerForm.get('phoneNumber').get('phone');
  }

  get address1(){
    return this.registerForm.get('address').get('address1');
  }

  get address2(){
    return this.registerForm.get('address').get('address2');
  }

  get city(){
    return this.registerForm.get('address').get('city');
  }

  get province(){
    return this.registerForm.get('address').get('province');
  }

  get zipCode(){
    return this.registerForm.get('address').get('zipCode');
  }

  get country(){
    return this.registerForm.get('address').get('country');
  }

  get month(){
    return this.registerForm.get('birth').get('month');
  }

  get day(){
    return this.registerForm.get('birth').get('day');
  }

  get year(){
    return this.registerForm.get('birth').get('year');
  }

  get checkArray(){
    return this.registerForm.get('checkArray');
  }

  
}
